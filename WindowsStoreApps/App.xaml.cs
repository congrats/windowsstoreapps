using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GalaSoft.MvvmLight.Threading;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WindowsStoreApps.Common.Settings;
using Windows.Storage;

namespace WindowsStoreApps
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.determineAppTheme();

            InitializeComponent();
            Suspending += OnSuspending;

        }

        /// <summary>
        /// Custom code to show how to read "setting" for theme and set it. This function is called in the app constructor.
        /// Refer to: http://code.msdn.microsoft.com/windowsapps/XAML-light-and-dark-app-da6affb0
        /// Note: If you want to determine app theme in application contructor, you should delete xmal's setting
        /// < Application RequestedTheme="Light" >
        /// </summary>
        private void determineAppTheme()
        {
            object oUseLightTheme = true;

            // Read the value of theme preference, if set. This value gets set when user changes theme in Scenario 2.
            if (ApplicationData.Current.LocalSettings.Values.TryGetValue("IsLightTheme", out oUseLightTheme))
            {
                if ((bool)oUseLightTheme == true)
                    this.RequestedTheme = ApplicationTheme.Light;
                else
                    this.RequestedTheme = ApplicationTheme.Dark;
            }
            else
                this.RequestedTheme = ApplicationTheme.Light;
        }

        private static WeakReference<Frame> _rootFrame = null;
        public static WeakReference<Frame> RootFrame
        {
            get { return _rootFrame; }
            set { _rootFrame = value; }
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected async override void OnLaunched(LaunchActivatedEventArgs args)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                // Registre the root frame in suspension manager
                Common.SuspensionManager.RegisterFrame(rootFrame, "AppRootFrame");
                // Keep the root frame's as global weak reference in application object
                RootFrame = new WeakReference<Frame>(rootFrame);

                if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //Load state from previously suspended application
                    await Common.SuspensionManager.RestoreAsync();
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                if (!rootFrame.Navigate(typeof(MainPage), args.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }

            // Ensure the current window is active
            Window.Current.Activate();
        }

        protected override void OnWindowCreated(WindowCreatedEventArgs args)
        {
            base.OnWindowCreated(args);

            /// Some initial things after creating of window
            DispatcherHelper.Initialize();
            GlobalAppSettingAgent.Default.InitWithCommandProvider(GlobalSettingCommandProvider.Default);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            // Save application state and stop any background activity
            // It only has about 5 seconds to do such keeping things
            await Common.SuspensionManager.SaveAsync();
            deferral.Complete();
        }
    }
}
