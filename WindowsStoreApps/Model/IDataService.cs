﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WindowsStoreApps.Model
{
    /// <summary>
    /// Define the generic data item class.
    /// Used to provided the abstracted data model for Data service interface.
    /// It provides two part:data item and exception error
    /// and the sequence as follow:
    /// <remarks>If the data service returns successfully, the item should not be null</remarks>
    /// <remarks>If the data service returns failed, the exception should not be null</remarks>
    /// <seealso cref="IDataService"/>
    /// </summary>
    /// <typeparam name="TItem">Generic data item type</typeparam>
    /// <typeparam name="TExp">Generic exception error type</typeparam>
    public class DateItemAndExcepton<TItem, TExp> 
        where TItem : class // Should be derived from object base class
        where TExp : Exception // Should be derived from System.Exception class
    {
        /// <summary>
        /// The data instance.
        /// It should not be null when data service return successfully
        /// <seealso cref="IDataService"/>
        /// </summary>
        public TItem Item;

        /// <summary>
        /// The exception error instance.
        /// It should not be null when data service reports error
        /// <seealso cref="IDataService"/>
        /// </summary>
        public TExp Exp;
    }

    /// <summary>
    /// Define the data service interface.
    /// It returns the call back data result or exception error.
    /// <seealso cref="DateItemAndExcepton"/>
    /// </summary>
    /// <typeparam name="TItem">Generic data item type</typeparam>
    /// <typeparam name="TExp">Generic exception error type</typeparam>
    public interface IDataService<TItem, TExp>
        where TItem : class // Should be derived from object base class
        where TExp : Exception // Should be derived from System.Exception class
    {
        /// <summary>
        /// Fetch data in sync mode
        /// </summary>
        /// <param name="callback">The callback action passed by data or exception error</param>
        void GetData(Action<TItem, TExp> callback);

        /// <summary>
        /// Fetch data in async mode
        /// </summary>
        /// <param name="token">The cancel token used to cancel current data feching task</param>
        /// <param name="callback">The callback action passed by the <seealso cref="DataItemAndException"/></param>
        /// <param name="completed">Completed action indicating that the overall fetching action has finished</param>
        /// <returns></returns>
        Task GetDataAsync(CancellationToken token, Action<DateItemAndExcepton<TItem, TExp>> callback, Action completed = null);
    }
}
