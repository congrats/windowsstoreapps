﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsStoreApps.Model
{
    public class MainPageDataItem : Common.BindableBase
    {
        public MainPageDataItem(string title, string description, string view)
        {
            Title = title;
            Description = description;
            View = view;
        }

        private string _title = string.Empty;
        public string Title
        {
            get { return _title; }
            set { this.SetProperty(ref _title, value, "Title"); }
        }

        private string _description = string.Empty;
        public string Description
        {
            get { return _description; }
            set { this.SetProperty(ref _description, value, "Description"); }
        }

        private string _view = string.Empty;
        public string View
        {
            get { return _view; }
            set { this.SetProperty(ref _view, value, "View"); }
        }

        private string _action = string.Empty;
        public string Action
        {
            get { return _action; }
            set { this.SetProperty(ref _action, value, "Action"); }
        }

        public override string ToString()
        {
            return string.Format(@"Title: {0}\r\n Description: {1}\r\n View: {2}",
                this.Title, this.Description, this.View);
        }
    }
}
