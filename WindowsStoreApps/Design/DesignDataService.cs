﻿using System;
using System.Threading;
using System.Threading.Tasks;
using WindowsStoreApps.Model;

namespace WindowsStoreApps.Design
{
    public class DesignDataService : IDataService<MainPageDataItem, Exception>
    {
        public void GetData(Action<MainPageDataItem, Exception> callback)
        {
            // Use this to create design time data

            var item = new MainPageDataItem("Network Practice", 
                "Some network tech points in windows store app.\n You can try to use those function in owner project.",
                "WindowsStoreApps.Views.NetworkView");
            callback(item, null);
        }



        public Task GetDataAsync(CancellationToken token, Action<DateItemAndExcepton<MainPageDataItem, Exception>> callback, Action completed = null)
        {
            throw new NotImplementedException();
        }
    }
}