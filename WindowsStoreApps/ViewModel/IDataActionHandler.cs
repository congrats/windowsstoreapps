﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsStoreApps.Model;

namespace WindowsStoreApps.ViewModel
{
    public interface IDataActionHandler
    {
        void HandleAction(MainPageDataItem item);
    }

    public class DataActionHandler : IDataActionHandler
    {
        private static readonly Lazy<IDataActionHandler> _handler = 
            new Lazy<IDataActionHandler>(
            () =>
            {
                return new DataActionHandler();
            }, true);

        public static IDataActionHandler Default
        {
            get
            {
                return _handler.Value;
            }
        }

        public void HandleAction(MainPageDataItem item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }

            Windows.UI.Xaml.Controls.Frame frame = null;
            if (!App.RootFrame.TryGetTarget(out frame) || !frame.Navigate(Type.GetType(item.View), item))
            {
                // Jump Failed!
                Debug.WriteLine("@@DataActionHandler => Jump failed!");
                Debug.WriteLine(item.ToString());
            }
        }
    }
}
