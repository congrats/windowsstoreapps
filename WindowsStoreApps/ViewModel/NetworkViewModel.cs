﻿using GalaSoft.MvvmLight;

namespace WindowsStoreApps.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class NetworkViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the NetworkView class.
        /// </summary>
        public NetworkViewModel()
        {
        }
    }
}