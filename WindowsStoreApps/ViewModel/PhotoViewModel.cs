﻿using GalaSoft.MvvmLight;

namespace WindowsStoreApps.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class PhotoViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the PhotoViewModel class.
        /// </summary>
        public PhotoViewModel()
        {
        }
    }
}