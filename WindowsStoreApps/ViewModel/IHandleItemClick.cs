﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsStoreApps.ViewModel
{
    public interface IHandleItemClick
    {
        void HandleItemClicked(object sender, Windows.UI.Xaml.Controls.ItemClickEventArgs e);
    }
}
