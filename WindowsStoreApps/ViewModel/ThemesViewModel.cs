﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Popups;

namespace WindowsStoreApps.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ThemesViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the ThemesViewModel class.
        /// </summary>
        public ThemesViewModel()
        {
            this.SwitchToLightCommand = new RelayCommand(
                () =>
                switchToLightTheme());

            this.SwitchToDarkCommand = new RelayCommand(
                () =>
                switchToDarkTheme());
        }

        private async void switchToDarkTheme()
        {
            // Store light or dark theme choice in app local settings. This value is read during app startup.
            ApplicationData.Current.LocalSettings.Values["IsLightTheme"] = false;

            // Changing the theme requires app restart. Notify user.
            MessageDialog md = new MessageDialog("Please restart the sample to see this theme applied to the output area below");
            await md.ShowAsync();
        }

        private async void switchToLightTheme()
        {
            // Store light or dark theme choice in app local settings. This value is read during app startup.
            ApplicationData.Current.LocalSettings.Values["IsLightTheme"] = true;
            
            // Changing the theme requires app restart. Notify user.
            MessageDialog md = new MessageDialog("Please restart the sample to see this theme applied to the output area below");
            await md.ShowAsync();
        }

        private RelayCommand _switchToLightCommand;
        public RelayCommand SwitchToLightCommand
        {
            get { return _switchToLightCommand; }
            set { this.Set("SwitchToLightCommand", ref _switchToLightCommand, value); }
        }

        private RelayCommand _switchToDarkCommand;
        public RelayCommand SwitchToDarkCommand
        {
            get { return _switchToDarkCommand; }
            set { this.Set("SwitchToDarkCommand", ref _switchToDarkCommand, value); }
        }
    }
}