﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;

namespace WindowsStoreApps.Common.Settings
{
    public interface IAppSettingAgent
    {
        void InitWithCommandProvider(ISettingCommandProvider provider);
        void TriggerSettingPanel();
    }

    public class GlobalAppSettingAgent : IAppSettingAgent
    {
        private static readonly Lazy<IAppSettingAgent> _agent =
            new Lazy<IAppSettingAgent>(
            () =>
            {
                return new GlobalAppSettingAgent();
            }, true);

        public static IAppSettingAgent Default
        {
            get
            {
                return _agent.Value;
            }
        }

        private ISettingCommandProvider _private = null;
        public void InitWithCommandProvider(ISettingCommandProvider provider)
        {
            if (_private != null)
            {
                SettingsPane.GetForCurrentView().CommandsRequested -= _private.CommandsRequested;
                _private = null;
            }

            _private = provider;
            SettingsPane.GetForCurrentView().CommandsRequested += _private.CommandsRequested;
        }


        public void TriggerSettingPanel()
        {
            SettingsPane.Show();
        }
    }
}
