﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace WindowsStoreApps
{
    public sealed partial class MainPage : Common.LayoutAwarePage
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 使用在导航过程中传递的内容填充页。在从以前的会话
        /// 重新创建页时，也会提供任何已保存状态。
        /// </summary>
        /// <param name="navigationParameter">最初请求此页时传递给
        /// <see cref="Frame.Navigate(Type, Object)"/> 的参数值。
        /// </param>
        /// <param name="pageState">此页在以前会话期间保留的状态
        /// 字典。首次访问页面时为 null。</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            // Reporting bug of mvvm light #7614: https://mvvmlight.codeplex.com/workitem/7614
            // If assign the setting command in this view through lambda, 
            // cycle reference will be created even use weak action(issue #7614 in WinRT version):
            // view -> dataContext -> viewmodel -> settingCommand -> Action(capture this as target) -> view

            /* 
            var locator = App.Current.Resources["Locator"] as ViewModel.ViewModelLocator;
            var mainVM = locator.Main;

            mainVM.SettingCommand = new GalaSoft.MvvmLight.Command.RelayCommand(() =>
                {
                    WindowsStoreApps.Common.Settings.GlobalAppSettingAgent.Default.TriggerSettingPanel();
                    var unused = Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
                        { System.Diagnostics.Debug.WriteLine("$$ Action invoked! " + this.Width); });
                });
             */
        }

        /// <summary>
        /// 保留与此页关联的状态，以防挂起应用程序或
        /// 从导航缓存中放弃此页。值必须符合
        /// <see cref="SuspensionManager.SessionState"/> 的序列化要求。
        /// </summary>
        /// <param name="pageState">要使用可序列化状态填充的空字典。</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private void itemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.DataContext is ViewModel.IHandleItemClick)
            {
                (this.DataContext as ViewModel.IHandleItemClick).HandleItemClicked(sender, e);
            }
        }

        ~MainPage()
        { 
        }
    }
}
