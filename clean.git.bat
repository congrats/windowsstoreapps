@echo off

rem Delete all .git folder in current folder recursively

for /r . %%a in (.) do @if exist "%%a\.git" rd /s /q "%%a\.git"

pause